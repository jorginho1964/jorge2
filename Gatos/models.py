from django.db import models
from django.utils import timezone





class Gato(models.Model):
    Id_Gato = models.AutoField(primary_key=True)
    Nombre = models.CharField(max_length=15)
    Color = models.CharField(max_length=25)
    Raza = models.CharField(max_length=25) 
    Caracteristicas = models.CharField(max_length=250)
    Fecha_Extravio = models.DateField()
    Region = models.CharField(max_length=100) 
    Sector = models.CharField(max_length=100)
    Fotografia = models.ImageField(upload_to='gallery',null=True,blank = True)
    Vigencia = models.BooleanField(default=True) 

    def __str__(self):
        txt = "NOMBRE DEL GATO: {0} (ID: {1}) - {2} " 
        if self.Vigencia:
            estado = "Extraviado"
        else:
            estado = "Encontrado"
        return txt.format(self.Nombre, self.Id_Gato,estado)
    

    
  


class Localizador(models.Model):
    Id_Localizador = models.AutoField(primary_key=True)
    Nombre_Localizador = models.CharField(max_length=40)
    Region_Localizador = models.CharField(max_length=100) 
    Ubicacion_Lozalizador = models.CharField(max_length=100)
    Telefono_Localizador = models.PositiveIntegerField()
    Correo_Electronico_L = models.CharField(max_length=40)
    Fecha_Localizador =  models.DateTimeField(
            default=timezone.now)
    Fotografia_Localizador = models.ImageField(upload_to='Localizados',null=True,blank = True)


    def publish(self):
        self.Fecha_localizado = timezone.now()
        self.save()

    def __str__(self):
        txt = "{0}  (Telefono:{1})"
        return txt.format(self.Nombre_Localizador, self.Telefono_Localizador)
     



class Dueño(models.Model):
    Nombres = models.CharField(max_length=40)  
    Primer_Apellido = models.CharField(max_length=15)
    Segundo_Apellido = models.CharField(max_length=15)
    gato = models.ForeignKey(Gato, null = False, blank=False, on_delete=models.CASCADE)
    Telefono = models.PositiveIntegerField()       
    Fecha =  models.DateTimeField(
            auto_now_add=True)
    Correo_Electronico = models.CharField(max_length=40)
         
    def __str__(self):
        txt = " {0} (Telefono: {1}) - {2} " 
        return txt.format(self.nombreCompleto(), self.Telefono, self.gato)    
   



    def nombreCompleto(self):
            txt = "{0} {1} {2}"
            return txt.format(self.Nombres,self.Primer_Apellido, self.Segundo_Apellido)
